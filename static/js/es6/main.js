/*

@author nando rossetto
@homepage https://github.com/nandorossetto
@e-mail nandorossetto@gmail.com

*/

const app = (function($) {
    let app = {
        toggleMenu: function(){ 
            $('.hamburger').on('click', function(){
                $(this).toggleClass('change');
                $('.header').toggleClass('open');
            });
        },

        validateFields: function(){
            $('#form').validate({
                errorPlacement: function(error,element) {
                    return false;
                },
                
                submitHandler: function() {
                    var el = $('.background, .message');
                    el.removeClass('hide');

                    setTimeout(function(){
                        el.addClass('hide');
                    }, 3000);
                },
                
                onfocusout: function(element) {
                    this.element(element);
                    
                    if($(element).is('.input-file')){
                        $(element).parents('.documents').find('label').removeClass('valid');
                        $(element).parents('.documents').find('label').addClass('error');
                    }

                    if($(element).is('.input-file.valid')){
                        $(element).parents('.documents').find('label').removeClass('error');
                        $(element).parents('.documents').find('label').addClass('valid');
                    }
                }
            });
        },

        parallax: function(){
            $(window).scroll(function() {
                var scrollTop = $(window).scrollTop();
                var position = '-' + scrollTop / 2 + 'px';

                $('.parallax').css('background-position', '0 ' + position);
            });
        },

        backToTop: function(){
            $('.back-top').on('click', function(){

                $('html, body').animate({
                    scrollTop: 0
                }, 700);

                return false;
            });
        },

        init: function(){
            app.toggleMenu();
            app.validateFields();
            app.parallax();
            app.backToTop();
        }
    };

    return {
        init: app.init
    }

}($));

app.init();