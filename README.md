Endouble-test
=========

To use it you'll need to have installed Node.js. So, if you don't, take a look at nodejs.org!
After that, if you don't have it, please install https://gruntjs.com/installing-grunt

Installation
--------------

```sh
npm install
```

##### Follow the Grunt plugins used

- uglify
- concat
- jshint
- compass
- watch

Available tasks
--------------

grunt dev: it'll watch your scss and js files to gives you a feedback about if have any error os if all fine

grunt deploy: will merge the static files to be ready to deploy
